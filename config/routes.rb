Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root 'pages#index'

  resources :links, only: [:create]
  get '/not_found', to: 'pages#not_found'
  get '/:token/info', to: 'links#info', as: 'link_info'
  get '/:token', to: 'links#show', as: 'show_link'
end
