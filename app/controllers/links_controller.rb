class LinksController < ApplicationController

  before_action :set_link, only: [:show, :info]

  def show
    TrackVisit.new(@link, request).call

    if @link
      redirect_to @link.long_url
    else
      redirect_to not_found_path
    end
  end

  def info
    # @analytics = AnalyticsService.new(@link).call
    @total_visits = LinkVisit.where(token: @link.token).count
    @geo_data = generate_geo_data

  rescue ActiveRecord::RecordNotFound
    redirect_to not_found_path
  end

  def create
    @link = UrlShortener.new(link_params).call

    if @link.persisted?
      redirect_to link_info_path(@link.token)
    else
      # redirect_to root_path
    end 
  end

  private

  def set_link
    @link = Link.find_by(token: params[:token])
  end

  def link_params
    params.require(:link).permit(:long_url)
  end

  def generate_geo_data
    result = LinkVisit.where(token: @link.token).count_group_by(:country_code).execute

    result["results"].map { |r| { r["_id"] => r.dig('value', 'count') }}
  end
end
