class Link < ApplicationRecord
  validates_presence_of :long_url, :token

  validates_uniqueness_of :token
end
