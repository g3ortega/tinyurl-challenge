class LinkVisit
  include Mongoid::Document
  include Mongoid::Timestamps

  field :token, type: String

  field :ip_address, type: String
  field :country_code, type: String
  field :referer, type: String

  index({ token: 1 })

  validates_presence_of :ip_address, :token

  def self.count_group_by(attr)
    map = %Q(
      function() {
        key = this.#{attr};
        value = { count: 1};
        emit(key, value);
      }
    )

    reduce = %Q(
      function(key, values) {
        var reducedValue = { count: 0 };
        values.forEach(function(value) {
          reducedValue.count += value.count
        });
        return reducedValue;
      }
    )

    self.map_reduce(map, reduce).out(inline: 1)
  end
end

