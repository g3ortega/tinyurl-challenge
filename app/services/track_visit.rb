class TrackVisit
  attr_reader :link, :request, :link_visit

  def initialize(link, request)
    @link = link
    @request = request
    @link_visit = LinkVisit.new(token: link.token)
  end

  def call
    create_link_visit
  end

  private

  def create_link_visit
    link_visit.ip_address = ip_address
    link_visit.country_code = country_code
    link_visit.referer = referer
    link_visit.save
  end

  def ip_address
    request.location.ip
  end

  def country_code
    request.location&.country&.strip
  end

  def referer
    request.referer
  end
end
