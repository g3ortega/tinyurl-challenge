class UrlShortener
  attr_reader :link

  def initialize(link_params)
    @link = Link.new(link_params)
  end

  def call
    save_token
  end

  private


  def save_token
    link.token = generate_uniq_token
    link.save
    link
  end

  def generate_uniq_token
    loop do
      token = SecureRandom.alphanumeric(6)

      return token if Link.where(token: token).count.zero? 
    end
  end
end
